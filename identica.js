
/**
 * Attach handlers to count the number of words in a given textfield, in its
 * description.
 */
$(document).ready(function() {
  $("#identica-textfield").keyup(function() {
    var charsLeft = (140 - $(this).val().length);
    var descDiv = $(this).parent().parent().parent().children(".description");
    $(descDiv).html("<strong>" + charsLeft + "</strong> characters remaining");
    if (charsLeft < 0) {
      $(descDiv).addClass("negative");
      $("#identica-post-button").attr('disabled', 'true');
    } else {
      $(descDiv).removeClass("negative");
      $("#identica-post-button").removeAttr('disabled');
    }
  });

  if (!$("#identica-toggle").attr("checked")) {
    $("#identica-textfield-wrapper").hide();
    $("#identica-account-wrapper").hide();
  }

  $("#identica-toggle").bind("click", function() {
    if ($("#identica-toggle").attr("checked")) {
      $("#identica-textfield-wrapper").show();
      $("#identica-account-wrapper").show();
    }
    else {
      $("#identica-textfield-wrapper").hide();
      $("#identica-account-wrapper").hide();
    }
  });
});
