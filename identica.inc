<?php

/**
 * @file
 * A wrapper API for the Identica microblogging service.
 *
 * Provides functions for retrieving public and authenticated messages from
 * Identica, helper code for locally caching Identica data and account
 * information, and utility functions for associating Drupal site users with
 * their Identica accounts.
 */


/**
 * Generate a identica posting form for the given user.
 *
 * @param $account
 *   A Drupal user object.
 */
function identica_form($account = NULL) {
  drupal_add_js(drupal_get_path('module', 'identica') .'/identica.js', 'module');

  if (empty($account)) {
    global $user;
    $account = $user;
  }

  $identica_accounts = drupal_map_assoc(array_keys(identica_get_user_accounts($account->uid, TRUE)));
  if (count($identica_accounts)) {
    $form = array();
    $form['status'] = array(
      '#type' => 'textfield',
      '#id' => 'identica-textfield',
    );

    if (count($identica_accounts) > 1) {
      $form['account'] = array(
        '#type' => 'select',
        '#title' => t('Account'),
        '#options' => $identica_accounts,
        '#access' => user_access(''),
        '#id' => 'identica-account',
      );
    }
    else {
      $form['account'] = array(
        '#type' => 'value',
        '#value' => array_pop(array_keys($identica_accounts))
      );
    }
    return $form;
  }
}



/**
 * Identica API functions
 */

/**
 * Fetch the public timeline for a Identi.ca account.
 *
 * Note that this function only requires a screen name, and not a password. As
 * such, it can only retrieve statuses for publically visible Identica accounts.
 * Because it doesn't require authentication, it is also easier on the Identica
 * servers. Be kind, use this version whenever you can.
 *
 * @param $screen_name
 *   The screen name of a Identi.ca user.
 * @param $filter_since
 *   A boolean indicating that Identica should only return statuses that have not
 *   been locally cached. This incurs an extra database hit, to retrieve the date
 *   of the most recent locally cached identica message for the screen name.
 * @param $cache
 *   A boolean indicating whether the statuses should be cached in the local
 *   site's database after they're retrieved.
 * @return
 *   An array of Identica statuses.
 *
 * @see identica_fetch_statuses()
 */
function identica_fetch_timeline($screen_name, $filter_since = TRUE, $cache = TRUE) {
  if ($filter_since) {
    $sql  = "SELECT t.created_at FROM {identica} t WHERE t.screen_name = '%s' ORDER BY t.created_at DESC";
    $since = db_result(db_query($sql, $screen_name));
  }

  $url = "http://identi.ca/api/statuses/user_timeline/$screen_name.xml";

  if (!empty($since)) {
    $url .= '?since='. urlencode($since);
  }

  $results = drupal_http_request($url, array(), 'GET');
  if (_identica_request_failure($results)) {
    return array();
  }
  else {
    $results = _identica_convert_xml_to_array($results->data);
    if ($cache) {
      foreach($results as $status) {
        identica_cache_status($status);
      }
      identica_touch_account($screen_name);
    }
    return $results;
  }
}


/**
 * Post a message to a Identi.ca account.
 *
 * @param $screen_name
 *   The screen name of a Identi.ca user.
 * @param $password
 *   The password of a Identi.ca user.
 * @param $text
 *   The text to post. Strings longer than 140 characters will be truncated by
 *   Identica.
 * @param $source
 *   A string indicating the program or site used to post the message. Source
 *   strings should be registered with Identica, as unrecgonized sources are
 *   ignored.
 * @return
 *   The full results of the Drupal HTTP request, including the HTTP response
 *   code returned by Identi.ca.
 */
function identica_set_status($screen_name, $password, $text, $source = 'drupal') {
  $url = "http://identi.ca/api/statuses/update.xml";

  $headers = array('Authorization' => 'Basic '. base64_encode($screen_name .':'. $password),
                   'Content-type' => 'application/x-www-form-urlencoded');
  $data = 'status='. urlencode($text);
  if (!empty($source)) {
    $data .= "&source=". urlencode($source);
  }

  return drupal_http_request($url, $headers, 'POST', $data);
}

/**
 * Fetch the full information for a Identi.ca account.
 *
 * This function requires an authenticated connection for the account in
 * question.
 *
 * @param $screen_name
 *   The screen name of a Identi.ca user.
 * @param $password
 *   The password of a Identi.ca user.
 * @param $cache
 *   A boolean indicating whether the account info should be cached in the local
 *   site's database after it's retrieved.
 * @return
 *   An single Identica account.
 */
function identica_fetch_account_info($screen_name, $password, $cache = TRUE) {
  $url = "http://identi.ca/api/users/show/$screen_name.xml";
  $headers = array('Authorization' => 'Basic '. base64_encode($screen_name .':'. $password),
                   'Content-type' => 'application/x-www-form-urlencoded');
  $results = drupal_http_request($url, $headers, 'GET');

  if (_identica_request_failure($results)) {
    return array();
  }

  if ($results = _identica_convert_xml_to_array($results->data)) {
    if ($cache) {
      foreach($results as $user) {
        identica_cache_account($user);
      }
    }
    return $results[0];
  }
  return array();
}

/**
 * Fetch the latest statuses for a Identi.ca account, regardless of privacy.
 *
 * This function is the authenticated version of identica_fetch_timeline(), and
 * is the only way to retrieve statuses for a 'private' account.
 *
 * @param $screen_name
 *   The screen name of a Identi.ca user.
 * @param $password
 *   The password of a Identi.ca user.
 * @param $cache
 *   A boolean indicating whether the statuses should be cached in the local
 *   site's database after they're retrieved.
 * @return
 *   An array of Identica statuses.
 *
 * @see identica_fetch_timeline()
 */
function identica_fetch_statuses($screen_name, $password, $cache = TRUE) {
  $url = "http://identi.ca/api/statuses/$screen_name.xml";
  $headers = array('Authorization' => 'Basic '. base64_encode($screen_name .':'. $password),
                   'Content-type' => 'application/x-www-form-urlencoded');

  $results = drupal_http_request($url, $headers, 'GET');
  if (_identica_request_failure($results)) {
    return array();
  }
  $results = _identica_convert_xml_to_array($results->data);

  if ($cache && !empty($results)) {
    foreach($results as $status) {
      identica_cache_status($status);
    }
    identica_touch_account($screen_name);
  }
  return $results;
}

/**
 * Fetch information about Identi.ca accounts followed by a given user.
 *
 * This function does not require authentication. It is mostly useful for mining
 * information about connections, and locating existing Identica friends who have
 * signed up for the same Drupal site.
 *
 * @param $screen_name
 *   The screen name of a Identi.ca user.
 * @return
 *   An array of Identica accounts.
 *
 * @see identica_fetch_followers()
 */
function identica_fetch_friends($screen_name) {
  $url = "http://identi.ca/api/statuses/friends/$screen_name.xml";
  $results = drupal_http_request($url, array(), 'GET');
  if (_identica_request_failure($results)) {
    return array();
  }
  return _identica_convert_xml_to_array($results->data);
}

/**
 * Fetch information about users following a given Identi.ca account.
 *
 * This function is mostly useful for mining information about connections, and
 * locating existing Identica friends who have also signed up for the same Drupal
 * site.
 *
 * @param $screen_name
 *   The screen name of a Identi.ca user.
 * @param $password
 *   The password of a Identi.ca user.
 * @return
 *   An array of Identica accounts.
 *
 * @see identica_fetch_friends()
 */
function identica_fetch_followers($screen_name, $password) {
  $url = "http://identi.ca/api/statuses/followers/$screen_name.xml";
  $headers = array('Authorization' => 'Basic '. base64_encode($screen_name .':'. $password),
                   'Content-type' => 'application/x-www-form-urlencoded');
  $results = drupal_http_request($url, $headers, 'GET');
  if (_identica_request_failure($results)) {
    return array();
  }
  return _identica_convert_xml_to_array($results->data);
}

/**
 * Attempts to authenticate a username/password on Identi.ca.
 *
 * @param $screen_name
 *   The screen name of a Identi.ca user.
 * @param $password
 *   The password of a Identi.ca user.
 * @return
 *   A boolean indicating success or failure.
 */
function identica_authenticate($screen_name, $password) {
  $url = "http://identi.ca/api/account/verify_credentials.xml";
  $headers = array('Authorization' => 'Basic '. base64_encode($screen_name .':'. $password),
                   'Content-type' => 'application/x-www-form-urlencoded');
  $results = drupal_http_request($url, $headers, 'GET');
  drupal_http_request('http://identi.ca/api/account/end_session', $headers, 'GET');
  return ($results->code == '200');
}

/**
 * Internal helper function to deal cleanly with various HTTP response codes.
 */
function _identica_request_failure($results) {
  switch ($results->code) {
    case '304':
      // Not modified, nothing to do.
      return TRUE;
    case 401:
    case 403:
      // Identica returns both of these for different classes of auth failure.
      watchdog('identica', 'Identica account could not be authenticated.');
      return TRUE;
    case 404:
      // Probably a bogus username.
      watchdog('identica', 'Identica account could not be retrieved.');
      return TRUE;
  }
  return FALSE;
}




/**
 * Caching functions
 */


/**
 * Saves Identica account information to the database.
 *
 * @param $identica_account
 *   A Identica user account in array form.
 *
 * @see identica_touch_account()
 * @see identica_cache_status()
 */
function identica_cache_account($identica_account = array()) {
  db_query("DELETE FROM {identica_account} WHERE identica_uid = %d", $identica_account['identica_uid']);
  drupal_write_record('identica_account', $identica_account);
}


/**
 * Updates the 'last refreshed on' timestamp of a given locally cached Identica
 * account.
 *
 * @param $screen_name
 *   A Identica screen name..
 *
 * @see identica_cache_account()
 * @see identica_cache_status()
 */
function identica_touch_account($screen_name = '') {
  db_query("UPDATE {identica_account} SET last_refresh = %d WHERE screen_name = '%s'", time(), $screen_name);
}


/**
 * Saves Identica status message to the database.
 *
 * If the $silent parameter is set to TRUE, this function will also notify other
 * modules via hook_identica_status_update() that a new stauts has been retrieved
 * and saved. This is normally set to FALSE, but may be useful when integrating
 * Identica into complex workflows.
 *
 * @param $status
 *   A Identica status updated in array form.
 * @param $silent
 *   A boolean indicating whether hook_identica_status_update should be fired.
 *
 * @see identica_touch_account()
 * @see identica_cache_status()
 */
function identica_cache_status($status = array(), $silent = FALSE) {
  db_query("DELETE FROM {identica} WHERE identica_id = %d", $status['identica_id']);
  drupal_write_record('identica', $status);
  if (!$silent) {
    module_invoke_all('identica_status_update', $status);
  }
}




/**
 * User/account relationship code
 */

function identica_get_user_accounts($uid, $only_with_passwords = FALSE) {
  $sql = "SELECT ta.*, tu.uid, tu.password, tu.import FROM {identica_user} tu LEFT JOIN {identica_account} ta ON (tu.screen_name = ta.screen_name) WHERE tu.uid = %d";
  if ($only_with_passwords) {
    $sql .= " AND tu.password IS NOT NULL";
  }
  $args = array($uid);
  $results = db_query($sql, $args);

  $accounts = array();
  while ($account = db_fetch_array($results)) {
    $accounts[$account['screen_name']] = $account;
  }
  return $accounts;
}

function identica_user_save($account = array(), $force_import = FALSE) {
  $account += array(
    'screen_name' => '',
    'import'      => 1,
  );

  if (db_result(db_query("SELECT 1 FROM {identica_user} WHERE uid = %d AND screen_name = '%s'", $account['uid'], $account['screen_name']))) {
    drupal_write_record('identica_user', $account, array('uid', 'screen_name'));  }
  else {
    drupal_write_record('identica_user', $account);
  }

  if ($force_import && $account['import']) {
    if (empty($account['protected']) || empty($account['password'])) {
      $statuses = identica_fetch_timeline($account['screen_name']);
    }
    else {
      identica_fetch_account_info($account['screen_name'], $account['password']);
      $statuses = identica_fetch_statuses($account['screen_name'], $account['password']);
    }
    
    if (!empty($statuses)) {
      identica_cache_account($statuses[0]['account']);
      identica_touch_account($account['screen_name']);
    }
  }
}

function identica_user_delete($uid, $screen_name = NULL) {
  $sql = "DELETE FROM {identica_user} WHERE uid = %d";
  $args = array($uid);
  if (!empty($screen_name)) {
    $sql .= " AND screen_name = '%s'";
    $args[] = $screen_name;
  }
  db_query($sql, $args);
}


/**
 * Internal XML munging code
 */

function _identica_convert_xml_to_array($data) {
  $results = array();
  $xml = new SimpleXMLElement($data);
  if (!empty($xml->name)) {
    // Top-level user information.
    $results[] = _identica_convert_user($xml);
    return $results;
  }
  if (!empty($xml->user)) {
    foreach($xml->user as $user) {
      $results[] = _identica_convert_user($user);
    }
  }
  elseif (!empty($xml->status)) {
    foreach($xml->status as $status) {
      $results[] = _identica_convert_status($status);
    }
  }
  return $results;
}

function _identica_convert_status($status) {
  $result = (array)$status;
  $result['identica_id'] = $result['id'];
  if (!empty($result['user']) && is_object($result['user'])) {
    $result['account'] = _identica_convert_user($result['user']);
    $result['screen_name'] = $result['account']['screen_name'];
  }
  else {
    $result['screen_name'] = NULL;
  }
  $result['created_time'] = strtotime($result['created_at']);
  return $result;
}

function _identica_convert_user($user) {
  $result = (array)$user;
  $result['identica_uid'] = $result['id'];
  if (!empty($result['status']) && is_object($result['status'])) {
    $result['status'] = _identica_convert_status($result['status']);
  }
  return $result;
}

function _identica_account_fields($user, $account = array()) {
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $user->uid,
  );
  $form['screen_name'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Identica user name'),
    '#default_value' => $identica['screen_name'],
  );
  $form['password'] = array(
    '#type' => 'password',
    '#required' => TRUE,
    '#title' => t('Password'),
    '#default_value' => $identica['password'],
  );

  return $form;
}
