<?php
/**
 * @file
 * Provide views data and handlers for identica.module
 */

/**
 * @defgroup views_identica_module identica.module handlers
 *
 * Includes the ability to create views of just the identica table.
 * @{
 */

/**
 * Implementation of hook_views_handlers()
 */
function identica_views_handlers() {
  return array(
    'handlers' => array(
      'identica_views_handler_field_profile_image' => array(
        'parent' => 'views_handler_field',
        'file' => 'identica_views_field_handlers.inc',
      ),
      'identica_views_handler_field_xss' => array(
        'parent' => 'views_handler_field',
        'file' => 'identica_views_field_handlers.inc',
      ),
    ),
  );
}


/**
 * Implementation of hook_views_data()
 */
function identica_views_data() {
  // Basic table information.

  $data['identica']['table']['group']  = t('Identica');

  // Advertise this table as a possible base table
  $data['identica']['table']['base'] = array(
    'field' => 'identica_id',
    'title' => t('Identica message'),
    'help' => t('Stores Identica status messages.'),
    'weight' => 10,
  );

  // Identica screen name
  $data['identica']['screen_name'] = array(
    'title' => t('Login name'),
    'help' => t('The login account of the Identica user.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Identica message timestamp
  $data['identica']['created_time'] = array(
    'title' => t('Created time'),
    'help' => t('The time the Identica message was posted.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // Identica text
  $data['identica']['text'] = array(
    'title' => t('Message text'),
    'help' => t('The text of the Identica message.'),
    'field' => array(
      'handler' => 'identica_views_handler_field_xss',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // Identica source
  $data['identica']['source'] = array(
    'title' => t('Source'),
    'help' => t('The name of the application that posted the Identica message.'),
    'field' => array(
      'handler' => 'views_handler_field_xss',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );




  $data['identica_account']['table']['group']  = t('Identica');

  $data['identica_account']['table']['join'] = array(
    'identica' => array(
      'left_field' => 'screen_name',
      'field' => 'screen_name',
    ),
    'users' => array(
      'field' => 'screen_name',
      'left_table' => 'identica_user',
      'left_field' => 'screen_name',
    ),
  );

  // Identica screen name
  $data['identica_account']['screen_name'] = array(
    'title' => t('Login name'),
    'help' => t('The login account of the Identica user.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );


  // Identica account full name
  $data['identica_account']['name'] = array(
    'title' => t('Full name'),
    'help' => t('The full name Identica account user.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Identica account description
  $data['identica_account']['description'] = array(
    'title' => t('Description'),
    'help' => t('The description of the Identica account.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_field_xss',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Identica account location
  $data['identica_account']['location'] = array(
    'title' => t('Location'),
    'help' => t('The location of the Identica account.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_field_xss',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Identica account description
  $data['identica_account']['followers_count'] = array(
    'title' => t('Location'),
    'help' => t('The number of users following this Identica account.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Identica account profile image
  $data['identica_account']['profile_image_url'] = array(
    'title' => t('Profile image'),
    'help' => t('The image used by the Identica account.'),
    'field' => array(
      'handler' => 'identica_views_handler_field_profile_image',
      'click sortable' => TRUE,
    ),
  );

  // Identica account url
  $data['identica_account']['url'] = array(
    'title' => t('URL'),
    'help' => t('The URL given by the Identica account user.'),
    'field' => array(
      'handler' => 'views_handler_field_url',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Identica account protected
  $data['identica_account']['protected'] = array(
    'title' => t('Protected status'),
    'help' => t('Whether posts from this Identica account should be visible to the general public.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Identica message timestamp
  $data['identica_account']['last_refresh'] = array(
    'title' => t('Last refresh'),
    'help' => t('The time the Identica account statuses were retrieved.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );


  $data['identica_user']['table']['group']  = t('Identica');

  $data['identica_user']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
    'identica_account' => array(
      'left_field' => 'screen_name',
      'field' => 'screen_name',
    ),
    'identica' => array(
      'left_table' => 'identica_account',
      'left_field' => 'screen_name',
      'field' => 'screen_name',
    ),
  );

  // Identica account description
  $data['identica_user']['uid'] = array(
    'title' => t('User ID'),
    'help' => t('The UID of the Identica account.'),
  );

  // Identica account description
  $data['identica_user']['screen_name'] = array(
    'title' => t('Screen name'),
    'help' => t('The screen name of the Identica account.'),
  );

  // Identica account protected
  $data['identica_user']['import'] = array(
    'title' => t('Import status'),
    'help' => t('Whether posts from this Identica account should be imported automatically.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );


  return $data;
}


function identica_views_data_alter(&$data) {
  $data['users']['table']['join']['identica'] = array(
    'left_table' => 'identica_user',
    'left_field' => 'uid',
    'field' => 'uid',
  );
  $data['users']['table']['join']['identica_account'] = array(
    'left_table' => 'identica_user',
    'left_field' => 'uid',
    'field' => 'uid',
  );
  $data['users']['table']['join']['identica_user'] = array(
    'left_field' => 'uid',
    'field' => 'uid',
  );
}

/**
 * @}
 */
